var imgId = "";
var urlImg = "http://localhost:56000/captcha";
var urlPost = "http://localhost:56000/validator"

function HttpPost(userTypedText) {
	const url =urlPost + "/" + imgId + "/" + userTypedText;
	const xhttp = new XMLHttpRequest();
	xhttp.open("POST", url, false);
	xhttp.send();

	const response = xhttp;
	
	if(response.status == 200) {
		if(window.confirm("Você acertou o Captcha!\nParabéns!!")){
			location.reload();
		}
	}
	else{
		window.alert("Você digitou muito mal. Tente novamente");
	}
	document.getElementById("value").value = "";
}

document.querySelector('button').onclick = function() {
    postCaptcha();
}

window.onload = function() {
	var url = urlImg;
	var xhttp = new XMLHttpRequest();
	xhttp.open("GET", urlImg, false);
	xhttp.send();

	var data = xhttp.responseText.split(";;;");

	var image = document.getElementById("captcha")
	var captchaImg = new Image();
	imgId = data[0];
	captchaImg.src = data[1];
	captchaImg.setAttribute("class", "rounded");
	captchaImg.setAttribute("alt", "200x100");

	image.parentNode.insertBefore(captchaImg, image);
  	image.parentNode.removeChild(image);
}

function postCaptcha() {
    var userTypedText = document.getElementById("value").value;
    HttpPost(userTypedText);
}
